import React from 'react';
import './App.css';
import { BrowserRouter, NavLink } from 'react-router-dom';
import { Routes } from './Routes';

function App() {

  return (
    <BrowserRouter>
      <NavLink to="/home">Home</NavLink> |
      <NavLink to="/user/1">users</NavLink> |
      <NavLink to="/posts">posts</NavLink> |
      <hr/>
      <Routes />
    </BrowserRouter>
  )

}
export default App;
