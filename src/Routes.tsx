import React, { lazy, Suspense } from 'react';
import { Route, Switch } from 'react-router';
import { HomePage } from './pages/home/homePage';
import { UserPage } from './pages/user/User';
import { Redirect } from 'react-router-dom';
const PostsPage = lazy(() => import('./pages/posts/PostsPage'))

export const Routes: React.FC = () => {
  return <Suspense fallback={<div>loading...</div>}>
    <Switch>
      <Route path="/home">
        <HomePage />
      </Route>

      <Route path="/user/:id" component={UserPage}  />

      <Route path="/posts">
        <PostsPage />
      </Route>

      <Route path="*">
        <Redirect to="/home" />
      </Route>
    </Switch>
  </Suspense>
};
