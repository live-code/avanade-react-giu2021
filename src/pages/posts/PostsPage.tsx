import React, { Dispatch, useMemo, useReducer, useState } from 'react';
import { PostsList } from './components/PostsList';
import { PostsForm } from './components/PostsForm';
import { usePosts } from './hooks/usePosts';

export type Theme = 'dark' | 'light';

export const AppContext = React.createContext<string>('pippo')
export const ThemeContext = React.createContext<Theme | null>(null)
export const DispatchContext = React.createContext<Dispatch<Action>>(() => null)

type Action = {
  type: 'changeCurrency',
  payload: '$' | '€'
}
function currencyReducer(state: '$' | '€', action: Action) {
  switch (action.type) {
    case 'changeCurrency':
      return action.payload
  }
}

const PostsPage: React.FC = () => {
  const { posts, http } = usePosts();
  const [count, setCount] = useState<number>(0); // created to simulate fake renders
  const [theme, setThemes] = useState<Theme>('light');
  const [currency, dispatch] = useReducer(currencyReducer, '$')

  console.log('render: PostPage')
  return (
    <AppContext.Provider value={currency}>
      <ThemeContext.Provider value={theme}>
        <DispatchContext.Provider value={dispatch}>

          <h1>Currenty: {currency}</h1>
              <button onClick={() => setCount(s => s+1)}>+</button>
              <button onClick={() => setThemes('dark')}>dark</button>
              <button onClick={() => setThemes('light')}>light</button>

              <div className="row">
                <div className="col">
                  <PostsForm onSave={http.submit} />
                </div>

                <div className="col">
                  <PostsList items={posts} onDeleteItem={http.delete}/>
                </div>


              </div>
        </DispatchContext.Provider>
    </ThemeContext.Provider>
    </AppContext.Provider>)
};

export default PostsPage;
