import React from 'react';
import { Post } from '../../../model/post';
import { useForm } from './useForm';
import { validateUserData } from '../utils/user.validator';

const initialFormData = { title: '', body: '', desc: ''};

export type FormPost = Omit<Post, 'id'>;

interface PostsFormProps {
  onSave?: (formData: FormPost) => void;
}
export const PostsForm: React.FC<PostsFormProps> = React.memo((props) => {
  const { formData, dirty, changeInput,  submit } = useForm<FormPost>(initialFormData, props.onSave);
  const { isValid, isTitleValid, isBodyValid } = validateUserData(formData);

  console.log('render: PostsForm')
  return (
    <form onSubmit={submit}>
      <input
        style={{ borderColor: !isTitleValid && dirty? 'red' : 'black'}}
        type="text" name="title" value={formData.title} onChange={changeInput}  placeholder="title"/>
      <input
        style={{ borderColor: !isBodyValid && dirty ? 'red' : 'black'}}
        type="text" name="body" value={formData.body} onChange={changeInput}  placeholder="body"/>

     <input
        type="text" name="desc" value={formData.desc} onChange={changeInput}  placeholder="desc"/>

      <button
        type="submit"
        disabled={!isValid}
      >ADD</button>
    </form>
  )
});
