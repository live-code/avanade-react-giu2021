import React, { useContext } from 'react';
import { Post } from '../../../model/post';
import { Card } from '../../../shared/Card';
import { AppContext, DispatchContext, ThemeContext } from '../PostsPage';

interface PostsListProps {
  items: Post[];
  onDeleteItem: (idx: number) => void
}

export const PostsList: React.FC<PostsListProps> = (props) => {

  return <>
    {
      props.items.map(p => {
        return <PostsListItem key={p.id} post={p} onDeleteItem={props.onDeleteItem} />
      })
    }
  </>
};


export interface PostsListItemProps {
  post: Post;
  onDeleteItem: (idx: number) => void
}
export const PostsListItem: React.FC<PostsListItemProps> = React.memo((props) => {
  const app = useContext(AppContext)
  const theme = useContext(ThemeContext)
  const dispatch = useContext(DispatchContext)
  return (
    <Card title={props.post.title}>
      {props.post.title} - ID: {props.post.id} - {theme} - {app}
      <i className="fa fa-trash"
         onClick={() => props.onDeleteItem(props.post.id)}/>
      <button onClick={() => dispatch({ type: 'changeCurrency', payload: '$'})}>$</button>
      <button onClick={() => dispatch({ type: 'changeCurrency', payload: '€'})}>€</button>
    </Card>
  )
})
