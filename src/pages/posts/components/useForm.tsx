import React, { useState } from 'react';

export function useForm<T>(
  initialValue: T,
  onSave?: (data: T) => void
) {

  const [formData, setFormData] = useState<T>(initialValue)
  const [dirty, setDirty] = useState<boolean>(false);

  function onChangeHandler(event: React.ChangeEvent<HTMLInputElement>) {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
    setDirty(true);
  }

  function saveHandler(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    if (onSave) {
      onSave(formData)
    }
  }

  return {
    formData,
    dirty,
    changeInput: onChangeHandler,
    submit: saveHandler
  }
}
