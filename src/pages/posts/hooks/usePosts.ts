import { useCallback, useEffect, useState } from 'react';
import { Post } from '../../../model/post';
import axios from 'axios';

export function usePosts() {
  const [posts, setPosts] = useState<Post[]>([]);

  useEffect(() => {
    getAllHandler();
  }, [])

  function getAllHandler() {
    axios.get<Post[]>('https://jsonplaceholder.typicode.com/posts')
      .then(res => setPosts(res.data))

  }

  const deleteHandler = useCallback((id: number) => {
    axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then(() => {
        setPosts(
          posts.filter(p => p.id !== id)
        )
      })
  }, [posts])

  const saveHandler = useCallback((formData: Omit<Post, 'id'>) => {
    axios.post<Post>(`https://jsonplaceholder.typicode.com/posts/`, formData)
      .then((res) => {
        setPosts(s => [...s, res.data])
      })
  }, [])

  return {
    posts,
    http: {
      get: getAllHandler,
      delete: deleteHandler,
      submit: saveHandler,
    }
  };
}
