import { Post } from '../../../model/post';

export function validateUserData(formData: Omit<Post, 'id'>) {

  const isTitleValid = formData.title !== '';
  const isBodyValid = formData.body !== '';
  const isValid = isTitleValid && isBodyValid;

  return {
    isValid,
    isTitleValid,
    isBodyValid,
  };
}
