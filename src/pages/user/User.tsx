import axios from 'axios';
import cn from 'classnames';
import React, { useCallback, useEffect, useState } from 'react';
import { User } from '../../model/user';
import { Card } from '../../shared/Card';
import { RouteComponentProps, useHistory, useParams } from 'react-router';

type Params = { id: string};

export function UserPage(props: RouteComponentProps<Params>) {
  console.log(props.match.params.id)
  const [user, setUser] = useState<User | null>(null)
  const [error, setError] = useState<boolean>(false)

  const { id } = useParams<Params>();
  const history = useHistory();


  function gotoNext() {
    history.push(`/user/${+id+1}`);
  }

  const getAll = useCallback(() => {
    setError(false)
    axios.get<User>('https://jsonplaceholder.typicode.com/users/' + id)
      .then(function(res) {
        setUser(res.data)
      })
      .catch(() => {
        setError(true)
      })
  }, [id])

  useEffect(() => {
    getAll();
  }, [getAll])

  return (
    <div>
      { error && <div className="alert alert-danger">error</div>}

      {
        user && (
          <Card
            title={user.name}>
            <div>{user.address.city}</div>
            <a href={'mailto:' + user?.email}>Contact me</a>
          </Card>
        )

      }

      <button onClick={gotoNext}>Next</button>
    </div>
  )

}

