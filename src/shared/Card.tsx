import React, { useState } from 'react';
import { User } from '../model/user';

interface CardProps {
  title: string;
}

export const Card: React.FC<CardProps> = (props) => {
  const [opened, setOpened] = useState<boolean>(true);
console.log('render: card')
  return (
    <div className="card">
      {<div className="card-header" onClick={() => setOpened(!opened)}>
        {props.title}
      </div>}
      {
        opened && (
          <div className="card-body">
            {props.children}
          </div>
        )
      }
    </div>
  )
};
